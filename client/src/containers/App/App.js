import React, {Component} from 'react';
import cx from 'classnames';
import SocketConnection from 'services/SocketConnection';
import Sidebar from 'components/Sidebar/Sidebar';
import TweetList from 'components/TweetList/TweetList';
import Loader from 'components/Loader/Loader';
import cs from './App.scss';

class App extends Component {
  state = {
    tweets: [],
    filter: {},
    searchParams: {},
    hashtag: null,
    resultType: null,
    loading: false
  };

  componentDidMount = () => {
    SocketConnection.on('new_tweet', this.onNewTweet);
    SocketConnection.on('search_finished', this.onSearchFinished);
  };

  onNewTweet = data => {
    if (this.state.tweets.length === 29) {
      this.onClickStreamStop();
    }

    this.setState(prevState => {
      const tweets = prevState.tweets;
      tweets.unshift(data.tweet);
      return {tweets};
    });
  };

  onSearchFinished = data => {
    if (data.searchFinished) {
      this.setState({
        loading: false
      });
    }
  };

  onChangeFilter = filter => {
    this.setState({filter});
  };

  onClickStream = streamParams => {
    console.log(streamParams, streamParams.filter.credibility.model);
    this.onClearTweets();
    streamParams.search.track = streamParams.search.q;
    SocketConnection.emit('stream_tweets', streamParams);
    this.setState({
      resultType: 'Streaming',
      loading: true,
      searchParams: streamParams
    });
  };

  onClickSearch = searchParams => {
    SocketConnection.emit('search_tweets', searchParams);
    this.onClearTweets();
    this.setState({resultType: 'Searching', loading: true, searchParams});
  };

  onClickStreamStop = () => {
    SocketConnection.emit('stop_search_tweets');
    this.setState({resultType: null, loading: false});
  };

  onClearTweets = () => {
    this.setState({
      tweets: []
    });
  };

  render() {
    const {tweets, filter, loading, searchParams, resultType} = this.state;

    return (
      <div className={cs.root}>
        <div className={cs.sidebar}>
          <Sidebar
            onClickStream={this.onClickStream}
            onClickStreamStop={this.onClickStreamStop}
            onClickSearch={this.onClickSearch}
            onChangeFilter={this.onChangeFilter}
            filter={filter}
            loading={loading}
          />
        </div>
        <div className={cs.content}>
          <div
            className={cx(cs.contentHeader, {
              [cs.contentHeader_visible]: loading || tweets.length > 0
            })}
          >
            <div className={cs.summary}>
              <Loader size="32px" loading={loading} />
              {loading &&
                tweets.length === 0 && (
                  <span>
                    Looking for tweets matching {searchParams.search.q}
                  </span>
                )}
              {tweets.length > 0 && (
                <span>
                  Results for {searchParams.search.q}:
                  <strong> {tweets.length} </strong>
                  {tweets.length === 1 ? 'tweet' : 'tweets'}.
                </span>
              )}
            </div>
          </div>
          {loading || resultType || tweets.length > 0 ? (
            <TweetList loading={loading} tweets={tweets} />
          ) : (
            <div className={cs.start}>
              <div className={cs.introduction}>
                <h2>
                  Exploring External Links in Twitter - Prototype Demonstration
                </h2>
                <p>
                  The goal of this prototype is to demonstrate the integration
                  of a machine learning model for credibility and sentiment
                  analysis employed on twitter data.
                </p>
                <p>
                  The prototype works with english tweets only. To get instant
                  stream results try to use a currently trending hashtag.
                </p>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default App;
