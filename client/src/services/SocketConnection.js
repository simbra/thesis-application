import io from 'socket.io-client';

//import {forwardTo} from '../utils/utils';

import {SOCKET_URL} from 'config/api';

class SocketConnection {
  constructor(url, reconnection = true, reconnectionDelay = 1000) {
    try {
      this.url = url;
      this.connected = false;
      this.socket = io(this.url, {
        reconnection: reconnection,
        reconnectionDelay: reconnectionDelay
      });

      this.socket.on('reconnect', () => {
        //dispatcher.dispatch({type: 'USER_SOCKET_RECONNECT'});
        //console.log('tjis socket id: ', this.socket.id);
      });

      this.socket.on('connect', () => {
        this.connected = true;
      });

      this.socket.on('disconected', () => {});
    } catch (err) {
      throw new Exception('cannot connect to socket server');
    }
  }

  disconnect() {
    this.socket.disconnect();
  }

  on(event, cb) {
    this.socket.on(event, cb);
  }

  emit(event, cb) {
    this.socket.emit(event, cb);
  }
}

const socketConnection = new SocketConnection(SOCKET_URL);
export default socketConnection;
