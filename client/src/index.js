import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import App from './containers/App/App';

ReactDOM.render(
  <BrowserRouter>
    <Route exact path="/" component={App} />
  </BrowserRouter>,
  document.getElementById('app')
);

/**
let jwt = localStorage.getItem('jwt');
if (jwt) {
LoginActions.loginUser(jwt);
}**/
