import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import cs from './Loader.scss';

const propTypes = {
  size: PropTypes.string,
  loading: PropTypes.bool.isRequired
};

const defaultProps = {
  size: '20px'
};

const Loader = ({size, loading}) => (
  <div
    className={cx(cs.root, {[cs.root_loading]: loading})}
    style={{width: size, height: size}}
  />
);

Loader.propTypes = propTypes;
Loader.defaultProps = defaultProps;
export default Loader;
