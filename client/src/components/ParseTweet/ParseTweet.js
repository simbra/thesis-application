import React from 'react';
import PropTypes from 'prop-types';

class TweetParser extends React.Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    classes: PropTypes.object.isRequired
  };

  urls = text => {
    const {classes} = this.props;
    return text.replace(
      /([-a-zA-Z0-9@:%_\+.~#?&//=]{1,256}\.[a-z]{2,4}\b)(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi,
      '<a href="$1$2" class=' + classes.urls + ' target="_blank">$1$2</a>'
    );
  };

  mentions = text => {
    const {classes} = this.props;
    return text.replace(
      /([@])([aA-zZ\d_\:]+)/gi,
      '<span class=' + classes.mentions + '>@$2</span>'
    );
  };

  hashtags = text => {
    const {classes} = this.props;
    return text.replace(
      /([#])([aA-zZ\d_\+]+)/gi,
      '<span class=' + classes.hashtags + '>#$2</span>'
    );
  };

  parse = () => {
    const text = this.props.text.substring(
      0,
      this.props.text.lastIndexOf(' ') + 1
    );
    let parsed = this.mentions(text);
    parsed = this.urls(parsed);
    return {__html: this.hashtags(parsed)};
  };

  render() {
    return <span dangerouslySetInnerHTML={this.parse()} />;
  }
}

export default TweetParser;
