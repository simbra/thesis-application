import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Checkbox from 'components/Checkbox/Checkbox';

import cs from './SearchForm.scss';

class SearchForm extends Component {
  static propTypes = {
    onClickStream: PropTypes.func.isRequired,
    onClickStreamStop: PropTypes.func.isRequired,
    onClickSearch: PropTypes.func.isRequired,
    onChangeFilter: PropTypes.func.isRequired,
    loading: PropTypes.bool
  };

  state = {
    searchInput: '#trump',
    filter: {
      sentiment: [
        {label: 'Positive', value: 'POSITIVE', checked: false},
        {label: 'Neutral', value: 'NEUTRAL', checked: false},
        {label: 'Negative', value: 'NEGATIVE', checked: false}
      ],
      credibility: [
        {label: 'Credible', value: 'CREDIBLE', checked: false},
        {label: 'Not Credible', value: 'NOT_CREDIBLE', checked: false}
      ],
      media: [
        {label: 'Photo', value: 'PHOTO', checked: false},
        {label: 'Video', value: 'VIDEO', checked: false}
      ]
    },
    models: {
      credibility: 'MediaEvalUser',
      sentiment: 'Afinn'
    },
    expert: false
  };

  updateFilter = (filter, target) => {
    return filter.map(filteritem => {
      if (target === filteritem.value) {
        filteritem.checked = !filteritem.checked;
      }
      return filteritem;
    });
  };

  getFilterObj = filterObj => {
    return filterObj.filter(item => item.checked === true).reduce(
      (newItem, item) => {
        newItem.values.push(item.value);
        newItem.labels.push(item.label);
        return newItem;
      },
      {values: [], labels: []}
    );
  };

  buildFilter = () => {
    const {filter, models} = this.state;
    const newFilter = {};
    Object.keys(filter).forEach(key => {
      newFilter[key] = this.getFilterObj(filter[key]);
      if (models[key]) {
        newFilter[key].model = models[key];
      }
    });

    return newFilter;
  };

  onClickStreamStop = e => {
    e.preventDefault();

    this.props.onClickStreamStop();
  };

  onClickStream = e => {
    e.preventDefault();

    const filter = this.buildFilter();
    const search = {
      q: this.state.searchInput
    };

    this.props.onClickStream({filter, search});
  };

  onClickSearch = e => {
    e.preventDefault();

    const filter = this.buildFilter();
    const search = {
      q: this.state.searchInput,
      result_type: 'mixed'
    };

    this.props.onClickSearch({filter, search});
  };

  onChangeSearchInput = e => {
    e.preventDefault();
    this.setState({searchInput: e.target.value});
  };

  onChangeFilterChechbox = filterObj => e => {
    const {onChangeFilter} = this.props;

    const chgFilter = this.updateFilter(filterObj, e.target.value);
    const filter = this.buildFilter();

    this.setState(
      {
        [filter]: chgFilter
      },
      () => {
        onChangeFilter(filter);
      }
    );
  };

  onChangeModelSelect = model => e => {
    const {models} = this.state;
    const newModels = {...models};
    Object.keys(models).forEach(key => {
      if (model === key) {
        newModels[key] = e.target.value;
      }
    });
    this.setState({models: newModels});
  };

  renderFilterCheckbox = (filter, cb) => {
    return filter.map(filteritem => {
      return (
        <Checkbox
          key={filteritem.label}
          label={filteritem.label}
          name={filteritem.label}
          checked={filteritem.checked}
          value={filteritem.value}
          labelClass={cx(cs.label, cs['label_' + filteritem.value])}
          checkboxClass={cs.checkbox}
          onChange={cb}
        />
      );
    });
  };

  render() {
    const {loading} = this.props;
    const {searchInput, filter, models, expert} = this.state;

    const isFormValid = searchInput.startsWith('#') && searchInput.length > 1;
    const searchFormDisabled = !!loading;

    const filterRowClassName = cx(cs.filterRow, {
      [cs.filterRow_overlay]: searchFormDisabled
    });

    return (
      <div className={cs.root}>
        <form>
          <div className={cs.filterForm}>
            <div className={filterRowClassName}>
              <label
                className={cx(cs.formlabel, {
                  [cs.formlabel_error]: !isFormValid
                })}
              >
                Hashtag
              </label>
              <input
                className={cs.input}
                type="text"
                value={searchInput}
                onChange={this.onChangeSearchInput}
              />
            </div>
          </div>
          <div className={cs.filterForm}>
            <div className={filterRowClassName}>
              <label className={cs.formlabel}>Media</label>
              {this.renderFilterCheckbox(
                filter.media,
                this.onChangeFilterChechbox(filter.media)
              )}
            </div>
          </div>
          <div className={cs.filterForm}>
            <div className={filterRowClassName}>
              <label className={cs.formlabel}>Sentiment</label>
              {this.renderFilterCheckbox(
                filter.sentiment,
                this.onChangeFilterChechbox(filter.sentiment)
              )}
            </div>
            <div
              className={cx(filterRowClassName, {
                [cs.filterRow_hidden]: !expert
              })}
            >
              <label className={cs.formlabel}>Package</label>
              <select
                value={models.sentiment}
                className={cs.select}
                onChange={this.onChangeModelSelect('sentiment')}
              >
                <option value="Sentiword">SentiWord</option>
                <option value="Afinn">Afinn</option>
              </select>
            </div>
          </div>
          <div className={cs.filterForm}>
            <div className={filterRowClassName}>
              <label className={cs.formlabel}>Crediblity</label>
              {this.renderFilterCheckbox(
                filter.credibility,
                this.onChangeFilterChechbox(filter.credibility)
              )}
            </div>
            <div
              className={cx(filterRowClassName, {
                [cs.filterRow_hidden]: !expert
              })}
            >
              <label className={cs.formlabel}>Model</label>
              <select
                value={models.credibility}
                className={cs.select}
                onChange={this.onChangeModelSelect('credibility')}
              >
                <option value="MediaEvalUser">MediaEvalUser BST</option>
                <option value="MediaEvalUser-snd">MediaEvalUser SND</option>
                <option value="MediaEvalUser-thd">MediaEvalUser THD</option>
              </select>
            </div>
          </div>
          <div className={cs.filterForm}>
            <div className={cs.filterRow}>
              <label className={cs.formlabel}>Search Mode</label>
              <div className={cs.actions}>
                <button
                  className={cs.button}
                  onClick={this.onClickStream}
                  disabled={searchFormDisabled || !isFormValid}
                  type="submit"
                  onSubmit={this.onClickStream}
                >
                  Stream
                </button>
                <button
                  className={cs.button}
                  onClick={this.onClickSearch}
                  disabled={searchFormDisabled || !isFormValid}
                >
                  Search
                </button>
                <button
                  className={cs.button}
                  onClick={this.onClickStreamStop}
                  disabled={!searchFormDisabled || !isFormValid}
                >
                  Stop Stream
                </button>
              </div>
            </div>
          </div>
          <div className={cs.filterForm}>
            <div className={cx(cs.filterRow, cs.filterRow_simple)}>
              <a
                className={cs.expert}
                onClick={() => {
                  this.setState(prevState => ({
                    expert: !prevState.expert
                  }));
                }}
              >
                {expert ? 'Hide' : 'Show'} advanced options
              </a>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default SearchForm;
