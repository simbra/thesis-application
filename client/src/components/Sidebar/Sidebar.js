import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import SearchForm from 'components/SearchForm/SearchForm';
import cs from './Sidebar.scss';

const propTypes = {
  onClickStream: PropTypes.func.isRequired,
  onClickStreamStop: PropTypes.func.isRequired,
  onClickSearch: PropTypes.func.isRequired,
  onChangeFilter: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  className: PropTypes.string
};

const Sidebar = ({
  onClickSearch,
  onClickStream,
  onClickStreamStop,
  onChangeFilter,
  className,
  loading
}) => (
  <div className={cx(className, cs.root)}>
    <div className={cs.header}>Search for Hashtag and set Filters</div>
    <SearchForm
      loading={loading}
      onClickStream={onClickStream}
      onClickStreamStop={onClickStreamStop}
      onClickSearch={onClickSearch}
      onChangeFilter={onChangeFilter}
    />
  </div>
);

Sidebar.propTypes = propTypes;
export default Sidebar;
