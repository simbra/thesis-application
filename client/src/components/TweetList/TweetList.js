import React from 'react';
import PropTypes from 'prop-types';
import TweetItem from 'components/TweetList/TweetItem';

import cs from './TweetList.scss';

const propTypes = {
  tweets: PropTypes.array,
  loading: PropTypes.bool
};

const TweetList = ({tweets, loading}) => (
  <div className={cs.root}>
    <div className={cs.list}>
      {tweets.length > 0 &&
        tweets.map(tweet => <TweetItem key={tweet.id} tweet={tweet} />)}

      {!loading &&
        tweets.length === 0 && (
          <h2 className={cs.emptyList}>
            Sorry, no tweets match your criteria.
          </h2>
        )}
    </div>
  </div>
);

TweetList.propTypes = propTypes;
export default TweetList;
