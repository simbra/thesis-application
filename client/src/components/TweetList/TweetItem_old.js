import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import NumFormat from '../../utils/NumFormat';
import ParseTweet from '../ParseTweet/ParseTweet';

import cs from './TweetItem.scss';

NumFormat.setLocale('en');

const propTypes = {
  tweet: PropTypes.object.isRequired
};


const TweetItem = ({ tweet }) => (
  <div className={cs.root}>
    <div className={cs.author}>
      <img
        alt="profile picutre"
        src={tweet.user.profile_image_url}
        className={cs.picture}
      />
    </div>
    <div className={cs.content}>
      <div className={cs.meta}>

        <a href={"https://twitter.com/" + tweet.user.screen_name} target="_blank" className={cs.name}>
          {tweet.user.name}
          {tweet.user.verified && <span className={cx(cs.verified)} />}
          <span className={cs.screenName}>@{tweet.user.screen_name}</span>
        </a>

        <span className={cs.followStats}>
          Has {NumFormat.format(tweet.user.followers_count)} followers and
          follows {NumFormat.format(tweet.user.friends_count)}
        </span>
      </div>
      <div className={cs.text}>
        <ParseTweet
          classes={{
            mentions: cs.mentions,
            hashtags: cs.hashtags,
            urls: cs.urls
          }}
          text={tweet.body}
        />
        <div className={cs.annotations}>
          <span
            title={tweet.__meta.sentiment.sentiment}
            className={cx(
              cs.icon,
              cs['icon_' + tweet.__meta.sentiment.sentiment.toLowerCase()]
            )}
          />
          <span
            title={tweet.__meta.credibility}
            className={cx(
              cs.icon,
              cs['icon_' + tweet.__meta.credibility.toLowerCase()]
            )}
          />
        </div>
        <a href={"https://twitter.com/statuses/" + tweet.id_str} target="_blank">
          <img alt={tweet.id_str} src={tweet._media.url} className={cs.media} />
        </a>
      </div>
      <div className={cs.options}>
        <a
          rel="noopener noreferrer"
          href={
            'https://www.google.com/searchbyimage?image_url=' + tweet._media.url
          }
          target="_blank"
        >
          Find origin of media
        </a>
      </div>
    </div>
  </div>
);

TweetItem.propTypes = propTypes;
export default TweetItem;
