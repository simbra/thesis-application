import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import TweetEmbed from 'react-tweet-embed';
import NumFormat from 'utils/NumFormat';
import cs from './TweetItem.scss';

NumFormat.setLocale('en');

const propTypes = {
  tweet: PropTypes.object.isRequired
};

const TweetItem = ({tweet}) => (
  <div
    className={cx(
      cs.root,
      cs['root_' + tweet.__meta.credibility.toLowerCase()]
    )}
  >
    <div className={cs.content}>
      <div className={cs.annotations}>
        <span
          title={tweet.__meta.credibility}
          className={cx(
            cs.icon,
            cs['icon_' + tweet.__meta.credibility.toLowerCase()]
          )}
        />
        <span
          title={tweet.__meta.sentiment.sentiment}
          className={cx(
            cs.icon,
            cs['icon_' + tweet.__meta.sentiment.sentiment.toLowerCase()]
          )}
        />
        <span className={cs.tweetType}>
          Type: {tweet.retweeted_status ? 'Retweet' : 'Original Content'}
        </span>
      </div>
      <div className={cs.meta}>
        <div className={cs.author}>
          <img
            alt="profile picutre"
            src={tweet.user.profile_image_url}
            className={cs.picture}
          />
        </div>
        <div className={cs.details}>
          <a
            href={'https://twitter.com/' + tweet.user.screen_name}
            target="_blank"
            className={cs.name}
          >
            {tweet.user.name}
            {tweet.user.verified && <span className={cx(cs.verified)} />}
            <span className={cs.screenName}>@{tweet.user.screen_name}</span>
          </a>

          <span className={cs.followStats}>
            {NumFormat.format(tweet.user.followers_count)} followers and follows{' '}
            {NumFormat.format(tweet.user.friends_count)}
          </span>
        </div>
      </div>

      <div className={cs.text}>
        <TweetEmbed
          id={tweet.id_str}
          options={{dnt: 'true', conversation: 'none'}}
        />
      </div>

      <div className={cs.options}>
        <a
          rel="noopener noreferrer"
          href={
            'https://www.google.com/searchbyimage?image_url=' + tweet._media.url
          }
          target="_blank"
        >
          Find origin of media
        </a>
      </div>
    </div>
  </div>
);

TweetItem.propTypes = propTypes;
export default TweetItem;
