import React from 'react';
import PropTypes from 'prop-types';
import cs from './Checkbox.scss';

const propTypes = {
  label: PropTypes.string,
  checkboxClass: PropTypes.string,
  labelClass: PropTypes.string,
  value: PropTypes.string,
  name: PropTypes.string,
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func
};

const Checkbox = ({
  label,
  checkboxClass,
  labelClass,
  value,
  name,
  checked,
  onChange
}) => {
  return (
    <div className={cs.root}>
      <input
        className={checkboxClass}
        type="checkbox"
        name={name}
        checked={checked}
        value={value}
        id={name}
        onChange={onChange}
      />
      <label className={labelClass} htmlFor={name}>
        {label}
      </label>
    </div>
  );
};

Checkbox.propTypes = propTypes;
export default Checkbox;
