let locale;

export default {
  setLocale(newLocale) {
    locale = newLocale;
  },
  format(num) {
    const formatter = new Intl.NumberFormat(locale);
    return formatter.format(num);
  }
};
