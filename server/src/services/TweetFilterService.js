import CredibilityClassifier from '../classification/credibility/CredibilityClassifier';
import {
  SENTIMENT_DICTS,
  SentimentClassifier
} from '../classification/sentiment-analysis/SentimentClassifier';

export const FILTER_TYPES = {
  MEDIA: 'media',
  SENTIMENT: 'sentiment',
  CREDIBILITY: 'credibility'
};

export class TweetFilterService {
  constructor() {
    this.tweet = null;
    this.meta = {};
    this.availFilter = [
      FILTER_TYPES.MEDIA,
      FILTER_TYPES.SENTIMENT,
      FILTER_TYPES.CREDIBILITY
    ];
    this.filter = [];
  }

  setTweet(tweet) {
    this.tweet = tweet;
    return this;
  }

  execFilter(type, options) {
    //this.tweet.__meta = {};
    if (this.availFilter.indexOf(type) !== -1) {
      this.filter.push({type: type + 'Filter', options});
    } else {
      throw 'Filter does not exist.';
    }

    return this;
  }

  _setMeta(type, value) {
    this.meta[type] = value;
  }

  async run() {
    let filterValue = true;
    let criteriaDoesNotMatch = {};

    for (let i = 0; i < this.filter.length; i++) {
      const filter = this.filter[i];
      const filterReturnedValue = await this[filter.type](filter.options);

      if (!filterReturnedValue) {
        criteriaDoesNotMatch = {filter: filter.type, options: filter.options};
        filterValue = false;
        break;
      }
    }

    const noMatch = {
      status: -1,
      criteriaDoesNotMatch,
      msg: 'No Tweet matches critiera'
    };

    this.filter = [];

    if (!filterValue) throw noMatch;

    const annotatedTweet = this.tweet;
    annotatedTweet.__meta = this.meta;

    return annotatedTweet;
  }

  mediaFilter(options) {
    if (!options) {
      return true;
    }

    if (
      (options.values.length === 0 && this.tweet._media) ||
      (this.tweet._media && options.values.indexOf(this.tweet._media.type) > -1)
    ) {
      return true;
    }
    return false;
  }

  sentimentFilter(options) {
    options.model = options.model || SENTIMENT_DICTS.AFINN;

    const sw = new SentimentClassifier(options.model);
    let sentiment;
    try {
      const sentimentObj = sw.getSentiment(this.tweet.text);

      sentiment = sentimentObj.score < 0 ? 'NEGATIVE' : 'POSITIVE';
      sentiment = sentimentObj.score === 0 ? 'NEUTRAL' : sentiment;

      if (
        options.values.length === 0 ||
        options.values.indexOf(sentiment) > -1
      ) {
        this._setMeta(FILTER_TYPES.SENTIMENT, {
          meta: sentimentObj,
          sentiment
        });
        return true;
      }
    } catch (e) {
      throw e;
    }
    return false;
  }

  async credibilityFilter(options) {
    if (!options) {
      return true;
    }

    try {
      const classifier = new CredibilityClassifier(options.model);
      const prediction = await classifier.classify(this.tweet);

      if (
        options.values.length === 0 ||
        options.values.indexOf(prediction) > -1
      ) {
        this._setMeta(FILTER_TYPES.CREDIBILITY, prediction);
        return true;
      }
    } catch (err) {
      throw err;
    }
    return false;
  }
}
