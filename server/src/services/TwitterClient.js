import Twitter from 'twitter';
import twitterConfig from '../config/twitter';
import LogUtils from '../utils/LogUtils';
import Tweet from '../models/Tweet';

const twitter = new Twitter(twitterConfig.api);

export default class TwitterClient {
  constructor(opts = {}) {
    this.client = twitter;
    this.openstream = null;
    this.opts = {
      language: 'en',
      replies: 'all',
      ...opts
    };
  }

  stream(params, cb) {
    this.streamHandler(
      this.client.stream('statuses/filter', {
        track: params.track,
        ...this.opts
      }),
      cb
    );
  }

  streamHandler(stream, cb) {
    this.openstream = stream;
    stream.on('error', err => {
      LogUtils.error('error ', err);
      cb(err);
    });

    stream.on('data', tweet => {
      cb(null, Tweet(tweet));
    });
  }

  search(params) {
    params.result_type = params.result_type || 'recent';
    params.lang = params.lang || 'en';
    params.count = params.count || 100;
    LogUtils.debug(params);
    return new Promise((resolve, reject) => {
      this.client.get('search/tweets', params, (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(
            results.statuses.map(tweet => {
              return Tweet(tweet);
            })
          );
        }
      });
    });
  }

  end() {
    if (this.openstream) {
      this.openstream.destroy();
    }
  }
}
