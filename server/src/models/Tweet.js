const getAllMedia = tweet => {
  let media = [];

  if (
    tweet.extended_tweet &&
    tweet.extended_tweet.extended_entities &&
    tweet.extended_tweet.extended_entities.media
  ) {
    media = media.concat(
      _getMediaFromPath(tweet.extended_tweet.extended_entities.media)
    );
  }

  if (tweet.extended_entities && tweet.extended_entities.media) {
    media = media.concat(_getMediaFromPath(tweet.entities.media));
  }

  return media;
};

const _getMediaFromPath = path => {
  let media = [];
  if (path) {
    media = path.filter(mediaEntry => {
      return mediaEntry.type === 'photo' || mediaEntry.type === 'video';
    });
  }
  return media;
};

const Tweet = tweet => {
  const annotatedTweet = tweet;
  if (tweet.text !== undefined) {
    annotatedTweet.body = tweet.text;
  }

  const media = getAllMedia(tweet);

  if (media.length >= 1) {

    if (media[0].expanded_url) {
      const matches = media[0].expanded_url.match(/\/video\/\d+/gi);

      if (matches) {
        annotatedTweet._media = {
          type: 'VIDEO',
          url: media[0].media_url_https
        };
      } else {
        annotatedTweet._media = {
          type: media[0].type.toUpperCase(),
          url: media[0].media_url_https
        };
      }
    }
  }

  return annotatedTweet;

};

export default Tweet;
