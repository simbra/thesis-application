export const FEATURE_SETS = {
  BST_ME: 'BST_ME',
  SND_ME: 'SND_ME',
  THD_ME: 'THD_ME'
};

export const SENTIMENT_DICTS = {
  AFINN: 'Afinn',
  SENTIWORD: 'Sentiword'
};

export const FILTER_TYPES = {
  MEDIA: 'media',
  SENTIMENT: 'sentiment',
  CREDIBILITY: 'credibility'
};
