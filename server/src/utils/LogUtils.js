export default {
  log: (...text) => {
    // eslint-disable-next-line no-console
    console.log(...text);
  },
  error: (...text) => {
    // eslint-disable-next-line no-console
    console.error(...text);
  },
  debug: (...args) => {
    // eslint-disable-next-line no-console
    if (process.env.DEBUG) console.info(new Date(), ...args);
  }
};
