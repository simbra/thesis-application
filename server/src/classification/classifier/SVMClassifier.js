import fs from 'fs';
import svm from 'node-svm';
import LogUtils from '../../utils/LogUtils';

export default class SVMClassifier {
  constructor(model, normalizeVector = true) {
    this.normalizeVector = normalizeVector;
    try {
      const data = JSON.parse(fs.readFileSync(model.data));
      this.svmModel = svm.restore(data);
      this.model = model;
    } catch (err) {
      throw 'Can not read model data: ' + err;
    }
  }

  normalize(featureVector) {
    const min = this.model.min;
    const max = this.model.max;

    for (let j = 0; j < featureVector.length; j++) {
      featureVector[j] = (featureVector[j] - min[j]) / (max[j] - min[j]);
    }

    return featureVector;
  }

  classify(featureVector) {
    if (this.normalizeVector) {
      featureVector = this.normalize(featureVector);
    }
    try {
      const prediction = this.svmModel.predictSync(featureVector);
      LogUtils.debug(
        'SMVClassifier',
        prediction,
        this.model.classes[prediction]
      );
      return this.model.classes[prediction];
    } catch (err) {
      throw err;
    }
  }
}
