import FeatureVector from './FeatureVector';

const requireModel = name => {
  try {
    const imp = require('./models/' + name + '/' + name);
    return imp.model;
  } catch (err) {
    throw 'Model ' + name + ' does not exist!';
  }
};

const requireClassifier = name => {
  try {
    const imp = require('../classifier/' + name);
    return imp.default;
  } catch (err) {
    throw 'Classifier ' + name + ' does not exist!';
  }
};

export default class CredibilityClassifier {
  constructor(model) {
    try {
      this.model = requireModel(model);
      this.classifier = requireClassifier(this.model.classifier);
    } catch (err) {
      throw err;
    }
  }

  async classify(tweet) {
    const fv = await FeatureVector(tweet, this.model.featureSet);
    const classifier = new this.classifier(this.model, this.model.normalize);
    const prediction = classifier.classify(fv);
    return prediction;
  }
}
