import path from 'path';

const MODEL_PATH = path.resolve(__dirname);

export const model = {
    min: [0, 0, 0, 0, 0, 0],
    max: [1, 1, 1, 1, 3, 2.1538461538461537],
    featureSet: 'THD_ME',
    normalize: 'true',
    classes: { 0: 'NOT_CREDIBLE', 1: 'CREDIBLE' },
    data: MODEL_PATH + '/svm-model-mediaeval-thd.dat',
    classifier: 'SVMClassifier'
};
