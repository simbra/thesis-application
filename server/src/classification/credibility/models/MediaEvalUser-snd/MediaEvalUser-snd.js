import path from 'path';

const MODEL_PATH = path.resolve(__dirname);

export const model = {
    min: [0, 0, 0, 0, 0, 0],
    max: [36202571, 366774, 1, 1, 1, 3],
    featureSet: 'SND_ME',
    normalize: 'true',
    classes: { 0: 'NOT_CREDIBLE', 1: 'CREDIBLE' },
    data: MODEL_PATH + '/svm-model-mediaeval-snd.dat',
    classifier: 'SVMClassifier'
};


