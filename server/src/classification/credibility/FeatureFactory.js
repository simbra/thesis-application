/*eslint import/namespace: ['error', { allowComputed: true }]*/
import * as features from './features/Features';

class FeatureFactory {
  constructor(tweet) {
    this.tweet = tweet;
  }

  value(featureName, opt) {
    if (!features[featureName]) {
      throw `Feature ${featureName} does not exist`;
    }
    if (opt === undefined) {
      return features[featureName](this.tweet);
    }
    return features[featureName](this.tweet, opt);
  }
}

export default FeatureFactory;
