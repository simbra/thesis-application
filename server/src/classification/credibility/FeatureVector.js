import {FEATURE_SETS} from '../../config';
import FeatureFactory from './FeatureFactory';

const FeatureVector = async (tweet, set) => {
  const feature = new FeatureFactory(tweet);
  const allWordsCount = feature.value('allWordsCount');
  let NER;

  try {
    switch (set) {
      case FEATURE_SETS.BST_ME:
        NER = await feature.value('NER');

        return [
          NER.hasRealPersonName,
          feature.value('hasGraphicProfilePicture'),
          feature.value('hashtagsMentionsCount') / allWordsCount,
          feature.value('uppercaseHashtagsCount') / allWordsCount,
          feature.value('linksCount'),
          (feature.value('uppercaseWordsCount') +
            feature.value('capitalizedWordsCount')) /
            allWordsCount
        ];

      case FEATURE_SETS.SND_ME:
        return [
          feature.value('followersCount'),
          feature.value('followingCount'),
          feature.value('hasLocation'),
          feature.value('hashtagsMentionsCount') / allWordsCount,
          feature.value('uppercaseHashtagsCount') / allWordsCount,
          feature.value('linksCount')
        ];

      case FEATURE_SETS.THD_ME:
        NER = await feature.value('NER');

        return [
          NER.hasRealPersonName,
          feature.value('hasGraphicProfilePicture'),
          feature.value('hashtagsMentionsCount') / allWordsCount,
          feature.value('uppercaseHashtagsCount') / allWordsCount,
          feature.value('linksCount')
        ];

      default:
        return [];
    }
  } catch (err) {
    throw err;
  }
};

export default FeatureVector;
