import hashtagsCount from './HashtagsCount';
import mentionsCount from './MentionsCount';

export default function getValue(tweet) {
  return hashtagsCount(tweet) + mentionsCount(tweet);
}
