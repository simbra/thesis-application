import request from 'Request'
import pos from 'pos';

function POStagValue(tweetPosTags, pos_tags) {
  var posTagValue = 0;
  if (pos_tags.constructor !== Array) {
    pos_tags = [pos_tags];
  }

  posTagValue = pos_tags.reduce((acc, tag) => {
    if (tweetPosTags[tag] !== undefined) {
      posTagValue += tweetPosTags[tag];
    }
    return posTagValue;
  }, 0)
  return posTagValue;
}

export default function getValue(tweet, params) {

  text = text.replace("RT", "");
  text = text.replace("&lt", "");
  text = text.replace("&gt", "");
  text = text.replace(/([@#])([aA-zZ\d_:\.\+]+)/ig, '');
  text = text.replace(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi, '');
  var words = new pos.Lexer().lex(text);
  var tagger = new pos.Tagger();
  var taggedWords = tagger.tag(words);

  var posTags = taggedWords.reduce((t, arr) => {
    var token = arr[1];
    switch (token) {
      case ".":
        token = 'dot';
        break;
      case ":":
        token = 'colon';
        break;
      case "PP$":
        token = 'PP_dollar';
        break;
      case "-":
        token = 'dash';
        break;
      case "_":
        token = 'lodash';
        break;
      case "$":
        token = 'dollar';
        break
      default:
        token = token;
        break;
    }
    if (t[token] !== undefined) {
      t[token] += 1;
    } else {
      t[token] = 1;
    }
    return t;
  }, {});
  if (params.tags) {
    return POStagValue(posTags, params.tags)
  } else {
    return 0
  }
}
