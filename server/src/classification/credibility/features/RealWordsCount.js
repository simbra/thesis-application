export default function getValue(tweet) {

  var text = tweet.body;
  // remove mentions and hashtags
  //var replaced = text;
  var replaced = text.replace(/([@#])([aA-zZ\d_:\.\+]+)/ig, '');
  // remove urls
  replaced = replaced.replace(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi, '');
  replaced = replaced.replace(/:/g, '');
  replaced = replaced.replace(/\s\s+/g, ' ');

  replaced = replaced.trim();
  return replaced.split(' ').length;
}
