import request from 'request';
import gateConfig from '../../../config/gatecloud';

function getApiUser() {
  return gateConfig.api.user + ':' + gateConfig.api.key;
}

export default function getValue(tweet) {
  const ne = {
    hasRealLocation: 0,
    hasRealPersonName: 0
  };

  return new Promise(function (resolve, reject) {
    let payload = '';

    if (tweet.user.location) {
      payload += tweet.user.location + ". ";
    } else if (tweet.user.name) {
      payload += tweet.user.name + ".";
    }

    if (payload) {
      request.post({
        url: 'https://' + getApiUser() + '@cloud-api.gate.ac.uk/process-document/twitie-named-entity-recognizer-for-tweets',
        headers: {
          'Content-Type': 'text/plain'
        },
        body: payload
      },
        function (err, httpResponse, body) {
          if (err) reject(err);

          body = JSON.parse(body);

          if (body.entities.Location !== undefined) {
            ne.hasRealLocation = 1;
          }

          if (body.entities.Person !== undefined) {
            ne.hasRealPersonName = 1;
          }

          resolve(ne);
        });
    } else {
      resolve(ne);
    }
  })

}