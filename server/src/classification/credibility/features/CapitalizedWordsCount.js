export default function getValue(tweet) {
  let text = tweet.body;
  // remove mentions and hashtags
  var capitalized = text.match(/([A-Z][\w]+)/g);
  if (capitalized == null) {
    return 0;
  }
  return capitalized.length;
}
