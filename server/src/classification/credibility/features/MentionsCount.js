export default function getValue(tweet) {
  // remove mentions and hashtags
  let text = tweet.body;
  var mentions = text.match(/([@])([aA-zZ\d_\:]+)/ig);
  if (mentions == null) {
    return 0;
  }
  return mentions.length;
}
