export default function getValue(tweet) {
  const text = tweet.body;
  return text.trim().split(' ').length;
}
