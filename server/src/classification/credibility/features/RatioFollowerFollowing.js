export default function getValue(tweet) {
  let user = tweet.user;
  if (user.followers_count === 0 || user.friends_count === 0) {
    return 0;
  }

  return user.followers_count / user.friends_count;
}
