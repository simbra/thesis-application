export default function getValue(tweet) {
  let text = tweet.body;
  let hashtags = text.match(/([#])([aA-zZ\d_\+]+)/gi);
  if (hashtags == null) {
    return 0;
  }
  return hashtags.length;
}
