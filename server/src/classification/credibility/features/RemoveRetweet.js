module.exports = {

    getValue: function (text) {
        var retweet = text.match(/((RT)|(RT:))/g);
        if (retweet != null) {
            if (retweet.length > 0) {
                var tweet = text.split(retweet[0]);

                return tweet[0];
            }
        }
        return '';
    }

}
