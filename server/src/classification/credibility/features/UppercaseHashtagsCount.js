export default function getValue(tweet) {

  let text = tweet.body;
  // remove mentions and hashtags([A-Z][\w]+([A-Z][\w]+)
  var hashtags = text.match(/([#])([A-Z]+)([\w\d_\+]+)/g);
  if (hashtags == null) {
    return 0;
  }

  return hashtags.length;
}
