export default function getValue(tweet) {
  return (!tweet.user.profile_image_url.endsWith('.jpg') ||
    !tweet.user.profile_image_url.endsWith('.JPG')) ? 0 : 1;
}
