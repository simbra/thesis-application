export default function getValue(tweet) {
  let user = tweet.user;
  return user.location != null && user.location !== '' ? 1 : 0;
}
