export default function getValue(tweet, params) {
  var text = tweet.body;
  if (params.hasOwnProperty('delimiter')) {
    return (text.split(params.delimiter).length - 1)
  }

  return 0;
}
