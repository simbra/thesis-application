export default function getValue(tweet) {

  let text = tweet.body;
  text = text.replace("RT", "");
  var uppercase_words = text.match(/\b[A-Z]+\b/g);
  uppercase_words = (uppercase_words != null) ? uppercase_words.length : 0;


  return uppercase_words;
}
