export default function getValue(tweet) {
  // remove mentions and hashtags
  var text = tweet.body;
  var url = text.match(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi);
  if (url == null) {
    return 0;
  }
  return (url.length > 0) ? url.length : 0;
}
