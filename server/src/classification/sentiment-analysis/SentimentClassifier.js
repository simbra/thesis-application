import Afinn from 'sentiment';
import Sentiword from 'sentiword';

export const SENTIMENT_DICTS = {
  AFINN: 'Afinn',
  SENTIWORD: 'Sentiword'
};

export class SentimentClassifier {
  constructor(dict) {
    this.availableDictionaries = [
      SENTIMENT_DICTS.AFINN,
      SENTIMENT_DICTS.SENTIWORD
    ];

    if (this.availableDictionaries.indexOf(dict) === -1) {
      throw 'Dictionary does not exist';
    }
    this.dict = dict;
  }

  getSentiment(text) {
    const sentiment = {meta: null, score: null};

    switch (this.dict) {
      case SENTIMENT_DICTS.AFINN:
        sentiment.meta = Afinn(text);
        sentiment.score = sentiment.meta.score;
        break;
      case SENTIMENT_DICTS.SENTIWORD:
        sentiment.meta = Sentiword(text);
        sentiment.score = sentiment.meta.sentiment;
        break;
      default:
        sentiment.score = null;
        break;
    }
    return sentiment;
  }
}
