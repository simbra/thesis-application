import api from './server';
import LogUtils from './utils/LogUtils';

const PORT = process.env.PORT || 3000;

api.listen(PORT, () => {
  LogUtils.log('[INFO] Listening on *:' + PORT);
  LogUtils.debug('DEBUG MODE ON');
});
