import Http from 'http';
import Koa from 'koa';
import Router from 'koa-router';
import Bodyparser from 'koa-bodyparser';
import Cors from 'kcors';
import SocketIO from 'socket.io';
import queue from 'queue';
import LogUtils from './utils/LogUtils';
import TwitterClient from './services/TwitterClient';
import {FILTER_TYPES, TweetFilterService} from './services/TweetFilterService';

const app = new Koa();
const api = new Router();

app
  .use(Cors())
  .use(Bodyparser())
  .use(api.allowedMethods())
  .use(api.routes());

api.get('/health', async ctx => {
  ctx.body = {success: true, message: 'alive'};
});

const server = Http.Server(app.callback());
const io = new SocketIO(server);

const filterService = new TweetFilterService();
const filterServiceCallback = async (tweet, filter) => {
  const matchedTweet = await filterService
    .setTweet(tweet)
    .execFilter(FILTER_TYPES.MEDIA, filter.media || undefined)
    .execFilter(FILTER_TYPES.SENTIMENT, filter.sentiment || undefined)
    .execFilter(FILTER_TYPES.CREDIBILITY, filter.credibility || undefined)
    .run()
    .catch(e => {
      throw e;
    });

  return matchedTweet;
};

io.on('connection', client => {
  const q = new queue();
  q.autostart = true;
  q.concurrency = 1;

  LogUtils.debug('client connected: ', client.id);

  const twitterClient = new TwitterClient();

  client.on('stream_tweets', async data => {
    LogUtils.debug(client.id + ': received stream: ', data);

    twitterClient.stream(data.search, (err, tweet) => {
      q.push(async cb => {
        const matchedTweet = await filterServiceCallback(
          tweet,
          data.filter
        ).catch(() => {});
        if (matchedTweet) {
          client.emit('new_tweet', {
            tweet: matchedTweet
          });
        }
        cb();
      });
    });
  });

  client.on('search_tweets', data => {
    const twitterClient = new TwitterClient();

    LogUtils.debug(client.id + ': received search: ', data);

    twitterClient
      .search(data.search)
      .then(async tweets => {
        for (let i = 0; i < tweets.length; i++) {
          const matchedTweet = await filterServiceCallback(
            tweets[i],
            data.filter
          ).catch(() => {});

          if (matchedTweet) {
            client.emit('new_tweet', {
              tweet: matchedTweet
            });
          }
        }

        client.emit('search_finished', {
          searchFinished: true
        });
      })
      .catch(e => {
        LogUtils.debug(e);
      });
  });

  client.on('stop_search_tweets', () => {
    LogUtils.debug('reveived stop');
    twitterClient.end();
    q.end();
  });

  client.on('disconnect', () => {
    LogUtils.debug('Dude disconnected');
    twitterClient.end();
    q.end();
  });
});

export default server;
